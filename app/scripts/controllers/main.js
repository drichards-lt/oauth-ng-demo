'use strict';

angular.module('newProjectApp')
  .controller('MainCtrl', function ($scope, $timeout, AccessToken, $rootScope) {
    $scope.$on('oauth:login', function(event, token, response) {
      console.log('yo')
      console.log('accessToken: ' + JSON.stringify(token));
      console.log('accessToken: ' + token.access_token);
      $rootScope.accessToken = token.access_token;
      $rootScope.oauthToken = token;
    });

    $scope.$on('oauth:logout', function(event) {
      $scope.accessToken = null;
    });
  });
